import jsminify from "gulp-js-minify"
import uglify from "gulp-uglify"
import concat from "gulp-concat"

export function js() {
    return app.gulp.src(app.path.src.js, { sourcemaps: app.isDev })
    .pipe(uglify())
    .pipe(app.plugins.if(app.isBuild, jsminify('app.min.js')))
    .pipe(app.plugins.if(app.isBuild, concat('app.min.js')))
    .pipe(app.gulp.dest(app.path.build.js) )
    .pipe(app.plugins.browsersync.stream());
}


