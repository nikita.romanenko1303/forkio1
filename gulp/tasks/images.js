import imagemin from "gulp-imagemin"

export const images = () => {
    return app.gulp.src(app.path.src.images) 
    .pipe (app.gulp.src(app.path.src.images))
    .pipe(app.plugins.if(app.isBuild, imagemin()))
    .pipe(app.plugins.if(app.isBuild, imagemin([
          imagemin.gifsicle({interlaced: true}),
          imagemin.mozjpeg({quality: 75, progressive: true}),
          imagemin.optipng({optimizationLevel: 5}),
          imagemin.svgo({
              plugins: [
                  {removeViewBox: true},
                  {cleanupIDs: false}
              ]
          })
        ])))
      
      .pipe(app.plugins.if(app.isBuild, app.gulp.dest(app.path.build.images)))
       
    .pipe(app.plugins.browsersync.stream())
      }