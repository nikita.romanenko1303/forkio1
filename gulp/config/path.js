const buildFolder = `./dist`
const srcFolder = `./src`

export const path = {
  build: {
    js: `${buildFolder}/js/`,
    css:`${buildFolder}/css/`,
    html:`${buildFolder}/`,
   files: `${buildFolder}/files/`,
   images: `${buildFolder}/images/`,
  },
  src: {
    js: `${srcFolder}/js/app.js`,
    html: `${srcFolder}/*.html`,
    files: `${srcFolder}/files/**/*.*`,
    scss: `${srcFolder}/scss/style.scss`,
    images: `${srcFolder}/images/**/*.{jpg,jpeg,png,gif,webp}`,
   },
  watch: {
    js: `${srcFolder}/js/**/*.js`,
    html: `${srcFolder}/**/*.html`,
   files: `${srcFolder}/files/**/*.*`,
   scss: `${srcFolder}/scss/**/*.scss`,
   images: `${srcFolder}/**/*.{jpg,jpeg,png,svg,gif,webp,ico}`,
  },
  clean: buildFolder,
  buildFolder: buildFolder,
  srcFolder: srcFolder,
  
}