# forkio1

Список використаних технологій:
- gulp
- npm
- scss
- git
- html

Cклад учасників проекту:
https://gitlab.com/nikita.romanenko1303
https://gitlab.com/maryna_hlushko

Завдання для студента Марина Глушко
Зверстала шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана).
Зверстала секцію People Are Talking About Fork.
Настроїла Gulp
 
Завдання для студента Нікіта Романенко
Зверстав блок Revolutionary Editor. 
Зверстав секцію Here is what you get.
Зверстав секцію Fork Subscription Pricing. 
Налаштував Git репзиторій

