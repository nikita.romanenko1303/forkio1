let menuBtn = document.querySelector('.header-burger');
let menu = document.querySelector('.header-menu-list');
menuBtn.addEventListener('click', function(){
    menuBtn.classList.toggle('active');
	menu.classList.toggle('active');
})

window.addEventListener('click', function (e) {
    if (!menu.contains(e.target) && ! menuBtn.contains(e.target)) {
        menuBtn.classList.remove('active');
        menu.classList.remove('active');
    }
});

let spanElement = document.querySelectorAll('.my__span__link');


spanElement.forEach(e => {
    e.addEventListener('click', function() {
        window.open('https://www.google.com/', '_blank');
    })
})